//check user buffer, what does it mean? can the user give a smaller buffer than the size of the channel?(128)
// currently read and write chech the private data attr, but it depends, if we open two files, with same device file, and only one
//does ioctl, does they both point to the same channel?
//need to do testing, open several driver files, several reads, writes, close one but still write in the other, etc.
// Declare what kind of code we want
// from the header files. Defining __KERNEL__
// and MODULE allows us to access kernel-level
// code not usually available to userspace programs.
#undef __KERNEL__
#define __KERNEL__
#undef MODULE
#define MODULE
#include <linux/kernel.h>   /* We're doing kernel work */
#include <linux/module.h>   /* Specifically, a module */
#include <linux/slab.h>		/* kmalloc */
#include <linux/fs.h>       /* for register_chrdev */
#include <asm/uaccess.h>    /* for get_user and put_user */
#include <linux/string.h>   /* for memset. NOTE - not string.h!*/
#include <stdbool.h>
MODULE_LICENSE("GPL");
//Our custom definitions of IOCTL operations
#include "message_slot.h"
//linked list of all channels
typedef struct channel_info
{
	int channel_id, cur_mes_len;
	char BUFFER[BUF_LEN];
	struct channel_info* next_channel;
}channel_info_t;
//linked list of opened_files
typedef struct open_file_info
{
	int minor;
	struct open_file_info* next_file;
	channel_info_t *first_channel;
	int num_channels;
}open_file_info_t;
// driver data struct
typedef struct message_slot_info
{
	int num_files;
	open_file_info_t* files_start;
}message_slot_info_t;
static message_slot_info_t driver_info;
//================== DEVICE FUNCTIONS ===========================
static int device_open(struct inode* inode,
	struct file*  file)
{
	//look for the minor file, if non, create
	int new_minor = iminor(inode);
	open_file_info_t* cur_file = driver_info.files_start;
	bool is_exist = false;
	for (size_t file_idx = 0; file_idx < driver_info.num_files; ++file_idx) {
		if (cur_file->minor == new_minor) {
			//we hold the channel id for the fd in the private data, set -1 for uninitialize
			file->private_data = (void*)-1;
			is_exist = true;
			printk(KERN_ALERT "%s already exist file driver, did not create new one  %d  %d\n",
				DEVICE_FILE_NAME, MAJOR_NUM, new_minor);
			return SUCCESS;
		}

		cur_file = cur_file->next_file;
	}

	//if no such minor, create and append
	if (!is_exist) {
		open_file_info_t *first_file = driver_info.files_start;
		driver_info.files_start = kcalloc(1, sizeof(open_file_info_t), GFP_KERNEL);
		if (driver_info.files_start == NULL)
		{
			printk(KERN_ALERT "%s could not allocate memory for new file  %d\n",
				DEVICE_FILE_NAME, MAJOR_NUM);
			return FAILURE;
		}

		driver_info.files_start->next_file = first_file;
		driver_info.files_start->minor = new_minor;
		driver_info.num_files++;
		//we created new file driver, no channels yet
		driver_info.files_start->first_channel = NULL;
		//we hold the channel id for the fd in the private data, set -1 for uninitialize
		file->private_data = (void*)-1;
		printk(KERN_ALERT "%s new file driver create %d  %d\n",
			DEVICE_FILE_NAME, MAJOR_NUM, new_minor);
	}

	return SUCCESS;
}

static int device_release(struct inode* inode,
	struct file*  file)
{
	return SUCCESS;
}

static ssize_t device_read(struct file* file,
	char __user* buffer,
	size_t       length,
	loff_t*      offset)
{
	//in ioctl we stored the channel id of the fd, if -1 means we did not set it yet
	int channel_id = (int)file->private_data;
	//no call to ioctl
	if (channel_id == -1) {
		printk(KERN_ALERT "%s channel id was not set to the file  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return -(EINVAL);
	}

	//search file driver with the minor, and the channel with the channel_id
	int minor_id = iminor(file->f_inode);
	bool is_exist = false;
	open_file_info_t* cur_file = driver_info.files_start;
	for (size_t file_idx = 0; file_idx < driver_info.num_files; ++file_idx) {
		if (cur_file->minor == minor_id) {
			is_exist = true;
			break;
		}
		cur_file = cur_file->next_file;
	}

	if (!is_exist) {
		printk(KERN_ALERT "%s could not find the minor id to read, error unkown  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return FAILURE;
	}

	//we now hold the relevant driver file
	is_exist = false;
	channel_info_t* cur_channel = cur_file->first_channel;
	for (size_t channel_idx = 0; channel_idx < cur_file->num_channels; ++channel_idx) {
		if (cur_channel->channel_id == channel_id) {
			is_exist = true;
			break;
		}
		cur_channel = cur_channel->next_channel;
	}

	if (!is_exist) {
		printk(KERN_ALERT "%s could not find channel inside the driver file should not happen  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return FAILURE;
	}

	//we now hold the relevant channel

	if (cur_channel->cur_mes_len > length) {
		printk(KERN_ALERT "%s not enough space in channel  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return -ENOSPC;
	}

	if (cur_channel->cur_mes_len == 0) {
		printk(KERN_ALERT "%s no message in channel  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return -EWOULDBLOCK;
	}

	for (size_t char_idx = 0; char_idx < cur_channel->cur_mes_len; char_idx++) {
		put_user(cur_channel->BUFFER[char_idx], buffer + char_idx);
	}

	*(buffer + cur_channel->cur_mes_len) = '\0';
	printk(KERN_ALERT "%s successfully read from device_file with channel id attached  %d %d %d\n",
		DEVICE_RANGE_NAME, MAJOR_NUM, cur_file->minor, cur_channel->channel_id);
	// return number of bytes read ( convention on read operations)
	return cur_channel->cur_mes_len;
}

static ssize_t device_write(struct file*       file,
	const char __user* buffer,
	size_t             length,
	loff_t*            offset)
{
	//in ioctl we stored the channel id of the fd, if -1 means we did not set it yet
	int channel_id = (int)file->private_data;
	//no call to ioctl
	if (channel_id == -1) {
		printk(KERN_ALERT "%s channel id was not set to the file  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return -(EINVAL);
	}

	//search file driver with the minor, and the channel with the channel_id
	int minor_id = iminor(file->f_inode);
	bool is_exist = false;
	open_file_info_t* cur_file = driver_info.files_start;
	for (size_t file_idx = 0; file_idx < driver_info.num_files; ++file_idx) {
		if (cur_file->minor == minor_id) {
			is_exist = true;
			break;
		}
		cur_file = cur_file->next_file;
	}

	if (!is_exist) {
		printk(KERN_ALERT "%s could not find the minor id to read, error unkown  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return FAILURE;
	}

	//we now hold the relevant driver file
	is_exist = false;
	channel_info_t* cur_channel = cur_file->first_channel;
	for (size_t channel_idx = 0; channel_idx < cur_file->num_channels; ++channel_idx) {
		if (cur_channel->channel_id == channel_id) {
			is_exist = true;
			break;
		}
		cur_channel = cur_channel->next_channel;
	}

	if (!is_exist) {
		printk(KERN_ALERT "%s could not find channel inside the driver file should not happen  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return FAILURE;
	}

	//we now hold the relevant channel

	if (length > BUF_LEN) {
		printk(KERN_ALERT "%s not enough space in channel  %d\n",
			DEVICE_RANGE_NAME, MAJOR_NUM);
		return -EINVAL;
	}

	memset(cur_channel->BUFFER, 0, BUF_LEN * sizeof(char));
	for (size_t char_idx = 0; char_idx < length; char_idx++) {
		get_user(cur_channel->BUFFER[char_idx], buffer + char_idx);
	}

	cur_channel->BUFFER[length] = '\0';
	cur_channel->cur_mes_len = length;
	printk(KERN_ALERT "%s successfully write to device_file with channel id attached  %d %d %d\n",
		DEVICE_RANGE_NAME, MAJOR_NUM, cur_file->minor, cur_channel->channel_id);
	// return number of bytes write ( convention on write operations)
	return length;
}

static long device_ioctl(struct   file* file,
	unsigned int   ioctl_command_id,
	unsigned long  ioctl_param)
{
	// Switch according to the ioctl called
	if (MSG_SLOT_CHANNEL == ioctl_command_id)
	{
		int channel_id = (int)ioctl_param;
		//we store the channel id for the fd inside the private_data
		file->private_data = (void*)channel_id;

		//find the relevant file driver
		int minor_id = iminor(file->f_inode);
		printk(KERN_ALERT "%s the minor id is  %d\n",
			DEVICE_RANGE_NAME, minor_id);
		bool is_exist = false;
		open_file_info_t* cur_file = driver_info.files_start;
		for (size_t file_idx = 0; file_idx < driver_info.num_files; ++file_idx) {
			if (cur_file->minor == minor_id) {
				is_exist = true;
				break;
			}
			cur_file = cur_file->next_file;
		}

		if (!is_exist) {
			printk(KERN_ALERT "%s could not find the minor id to allocate channel, error unkown  %d\n",
				DEVICE_RANGE_NAME, MAJOR_NUM);
			return FAILURE;
		}

		//we now hold the relevant driver file

		//we now search for the channel, if none, create new channel
		is_exist = false;
		channel_info_t* cur_channel = cur_file->first_channel;
		for (size_t channel_idx = 0; channel_idx < cur_file->num_channels; ++channel_idx) {
			if (cur_channel->channel_id == channel_id) {
				is_exist = true;
				break;
			}
			cur_channel = cur_channel->next_channel;
		}

		if (!is_exist) {
			channel_info_t* first_channel = cur_file->first_channel;
			cur_file->first_channel = kcalloc(1, sizeof(channel_info_t), GFP_KERNEL);
			cur_file->first_channel->next_channel = first_channel;
			cur_file->first_channel->channel_id = channel_id;
			cur_file->num_channels++;
			printk(KERN_ALERT "%s successfully attached device_file with channel id  %d %d %d\n",
				DEVICE_RANGE_NAME, MAJOR_NUM, cur_file->minor, cur_file->first_channel->channel_id);
		}

		return SUCCESS;
	}

	printk(KERN_ALERT "%s channel id was not set to the file unknown error  %d\n",
		DEVICE_RANGE_NAME, MAJOR_NUM);
	return -EINVAL;
}

struct file_operations Fops =
{
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.unlocked_ioctl = device_ioctl,
	.release = device_release,
};
static int __init message_slot_init(void)
{
	int rc = -1;
	// init dev struct
	memset(&driver_info, 0, sizeof(message_slot_info_t));
	// Register driver capabilities. Obtain major num
	rc = register_chrdev(MAJOR_NUM, DEVICE_RANGE_NAME, &Fops);
	// Negative values signify an error
	if (rc < 0)
	{
		printk(KERN_ALERT "%s registraion failed for  %d\n",
			DEVICE_FILE_NAME, MAJOR_NUM);
		return rc;
	}

	printk(KERN_INFO "message_slot: registered major number %d\n", MAJOR_NUM);
	return 0;
}

static void __exit message_slot_cleanup(void)
{
	// Unregister the device
	// Should always succeed
	open_file_info_t *root_file = driver_info.files_start;
	for (size_t file_idx = 0; file_idx < driver_info.num_files; ++file_idx) {
		channel_info_t *root_channel = root_file->first_channel;
		for (size_t channel_idx = 0; channel_idx < root_file->num_channels; ++channel_idx) {
			channel_info_t* next_channel = root_channel->next_channel;
			kfree(root_channel);
			root_channel = next_channel;
		}


		open_file_info_t* next_file = root_file->next_file;
		kfree(root_file);
		root_file = next_file;
	}

	unregister_chrdev(MAJOR_NUM, DEVICE_RANGE_NAME);
	printk("Unregister is successful. ");
}

module_init(message_slot_init);
module_exit(message_slot_cleanup);
